 
FROM golang:1.19.1-alpine3.16 as builder
 
WORKDIR /post

COPY . .

RUN go build -o main cmd/main.go

FROM alpine:3.16

WORKDIR /post

COPY --from=builder /post/main .

CMD [ "/post/main" ]